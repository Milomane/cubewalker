﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectionMenu : MonoBehaviour
{
    public GameObject gears;
    
    void Start()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");
        gears.GetComponent<Gears>().soundManager.GetComponent<SoundsManager>().PlaySound("Menu");
    }
    
    void Update()
    {
        if (Input.GetButtonDown("Submit"))
        {
            gears.GetComponent<Gears>().LoadGame();
            Destroy(gameObject);
        }
    }
}
