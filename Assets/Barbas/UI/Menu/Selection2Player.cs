﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Selection2Player : MonoBehaviour
{
    public GameObject gears;
    
    public string inputPlayer1;
    public string inputPlayer2;

    public bool player1Ready;
    public bool Player2Ready;

    public GameObject screenPlayer1Ready;
    public GameObject screenPayer2Ready;

    public GameObject posScreenPlayer1;
    public GameObject posScreenPlayer2;


    private GameObject go1;
    private GameObject go2;

    public SpriteRenderer timer;
    public bool firstActiv;
    
    void Start()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");
        gears.GetComponent<Gears>().soundManager.GetComponent<SoundsManager>().PlaySound("Menu");
        timer = GetComponent<SpriteRenderer>();
    }
    
    void Update()
    {
        if (Input.GetButtonDown(inputPlayer1) && !player1Ready)
        {
            player1Ready = true;
            go1 = Instantiate(screenPlayer1Ready, gears.GetComponent<Gears>().canvasMain.transform);
            go1.GetComponent<RectTransform>().localPosition = posScreenPlayer1.GetComponent<RectTransform>().localPosition + new Vector3(0,-150,0);
        }
        
        if (Input.GetButtonDown(inputPlayer2) && !Player2Ready)
        {
            Player2Ready = true;
            go2 = Instantiate(screenPayer2Ready, gears.GetComponent<Gears>().canvasMain.transform);
            go2.GetComponent<RectTransform>().localPosition = posScreenPlayer2.GetComponent<RectTransform>().localPosition + new Vector3(0,-150,0);
        }

        if (player1Ready && Player2Ready && !firstActiv)
        {
            timer.DOColor(timer.color, 0.5f).OnComplete(Timer);
            firstActiv = true;
        }
    }

    public void Timer()
    {
        Destroy(go1);
        Destroy(go2);
        gears.GetComponent<Gears>().LoadGame();
        player1Ready = false;
        Player2Ready = false;
        firstActiv = false;
    }
}
