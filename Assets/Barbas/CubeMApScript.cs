﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMApScript : MonoBehaviour
{
    public GameObject gears;
    
    void Start()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");

        gears.GetComponent<Gears>().cameraManager.GetComponent<CameraScript>().centerOfTheCube3 = transform.position;
    }
}
