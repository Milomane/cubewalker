﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class QTEManager : MonoBehaviour
{
    public GameObject gears;

    public Transform showTextHere;
    public Transform showTextHerePlayer2;

    public List<QTEClass> test = new List<QTEClass>();
    public List<QTEClass> player2QTE = new List<QTEClass>();
    public QTEClass[] possibleInput;

    public GameObject feedBackPosWorldPlayer1;
    private GameObject feedBackInGamePlayer1;
    
    public GameObject feedBackPosWorldPlayer2;
    private GameObject feedBackInGamePlayer2;

    public GameObject[] feedBackQuality;

    public float fixedPercent;
    public bool upGrading;
    public bool firstActive;
    
    public float fixedPercentPlayer2;
    public bool upGradingPlayer2;
    public bool firstActivePlayer2;

    void Start()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");
        gears.GetComponent<Gears>().qTEManager = gameObject;
    }

    void FixedUpdate()
    {
        fixedPercent = gears.GetComponent<Gears>().player1.GetComponent<Player>().percentOfHit;
        fixedPercentPlayer2 = gears.GetComponent<Gears>().player2.GetComponent<Player>().percentOfHit;
    }
    
    void Update()
    {
        if (Input.GetButtonDown("Submit"))//test
        {
           
        }
        
        int i = 0; //i = le nombre d'input actife                                                                           JOUEUR 1
        int a = 0; //le nombres d'input finit

        foreach (var qte in test)
        {
            if (!qte.done)
            {
                if (i == 0) //montrer/cachée les qte
                {
                    if (!qte.textShowedBool)
                    {
                        qte.inputShowed = Instantiate(qte.showInput);
                        qte.inputShowed.transform.SetParent(gears.GetComponent<Gears>().canvasMain.transform);
                        qte.textShowedBool = true;   
                    }
                    Vector2 inputPos = Camera.main.WorldToScreenPoint(feedBackPosWorldPlayer1.transform.position);
                    qte.inputShowed.GetComponent<RectTransform>().position = inputPos + new Vector2(60, 0);
                }
                else if (i == 1)
                {
                    if (!qte.textShowedBool)
                    {
                        qte.inputShowed = Instantiate(qte.showInput);
                        qte.inputShowed.transform.SetParent(gears.GetComponent<Gears>().canvasMain.transform);
                        qte.textShowedBool = true;   
                    }
                    Vector2 inputPos2 = Camera.main.WorldToScreenPoint(feedBackPosWorldPlayer1.transform.position );
                    qte.inputShowed.GetComponent<RectTransform>().position = inputPos2 + new Vector2(-60, 0);
                }
            }
            else
            {
                Destroy(qte.inputShowed);
                a++;
            }
            i++;
        }

        if (test.Count == 2)
        {
            if (Input.GetButtonDown(test[0].input) && !test[0].done) //si ont appuyie sur la touche définie
            {
                int M0 = 0; //juste pour tous changer en même temps

                Percent(M0, test, true, gears.GetComponent<Gears>().player1);
            }
        }

        if (test.Count == 2)
        {
            if (Input.GetButtonDown(test[1].input) && !test[1].done)
            {
                int M1 = 1;

                Percent(M1, test, true, gears.GetComponent<Gears>().player1);
            }
        }

        if (a == 2)
        {
            print("player1 done");
            ClearList(test);
        }
                                                                                             //JOUEUR 2
        int iP2 = 0; //i = le nombre d'input actife
        int aP2 = 0; //le nombres d'input finit
        
        foreach (var qte in player2QTE)
        {
            if (!qte.done)
            {
                if (iP2 == 0 && !qte.textShowedBool) //montrer/cachée les qte
                {
                    if (!qte.textShowedBool)
                    {
                        qte.inputShowed = Instantiate(qte.showInput);
                        qte.inputShowed.transform.SetParent(gears.GetComponent<Gears>().canvasMain.transform);
                        qte.textShowedBool = true;   
                    }
                    Vector2 inputPos = Camera.main.WorldToScreenPoint(feedBackPosWorldPlayer2.transform.position);
                    qte.inputShowed.GetComponent<RectTransform>().position = inputPos + new Vector2(60, 0);
                }
                else if (iP2 == 1 && !qte.textShowedBool)
                {
                    if (!qte.textShowedBool)
                    {
                        qte.inputShowed = Instantiate(qte.showInput);
                        qte.inputShowed.transform.SetParent(gears.GetComponent<Gears>().canvasMain.transform);
                        qte.textShowedBool = true;   
                    }
                    Vector2 inputPos2 = Camera.main.WorldToScreenPoint(feedBackPosWorldPlayer2.transform.position );
                    qte.inputShowed.GetComponent<RectTransform>().position = inputPos2 + new Vector2(-60, 0);
                }
            }
            else
            {
                Destroy(qte.inputShowed);
                aP2++;
            }
            iP2++;
        }

        if (player2QTE.Count == 2)
        {
            if (Input.GetButtonDown(player2QTE[0].input) && !player2QTE[0].done) //si ont appuyie sur la touche définie
            {
                int L0 = 0;
                
                Percent(L0, player2QTE, false, gears.GetComponent<Gears>().player2);
            }
        }

        if (player2QTE.Count == 2)
        {
            if (Input.GetButtonDown(player2QTE[1].input) && !player2QTE[1].done)
            {
                int L1 = 1; //L = luigie
                
                Percent(L1, player2QTE, false, gears.GetComponent<Gears>().player2);
            }
        }

        if (aP2 == 2)
        {
            print("player2 done");
            ClearList(player2QTE);
        }
    }

    void LateUpdate()
    {
        if (gears.GetComponent<Gears>().player1.GetComponent<Player>().percentOfHit < fixedPercent || gears.GetComponent<Gears>().player1.GetComponent<Player>().percentOfHit == 10)   //nouveaux inputs
        {
            upGrading = true; //se raproche du point
        }
        else
        {
            upGrading = false;
            firstActive = false;
        }

        if (upGrading && !firstActive)
        {
            QTEClass addInput = new QTEClass(gears.GetComponent<Gears>().player1.GetComponent<Player>().currentInputName, possibleInput[0].showInput);//,
                //possibleInput[0].failD, possibleInput[0].goodD,possibleInput[0].greatD, possibleInput[0].perfectD, possibleInput[1].speedsGood, possibleInput[1].speedsGreat, possibleInput[1].speedsPerfect);
            
            QTEClass addInputQ = new QTEClass(gears.GetComponent<Gears>().player1.GetComponent<Player>().currentInputName, possibleInput[1].showInput);

            AddInputToPlayer(addInput, test);
            AddInputToPlayer(addInputQ, test);

            firstActive = true;
        }

        if (gears.GetComponent<Gears>().player2.GetComponent<Player>().percentOfHit < fixedPercentPlayer2 || gears.GetComponent<Gears>().player2.GetComponent<Player>().percentOfHit == 10)   //nouveaux inputs joueur 2
        {
            upGradingPlayer2 = true;
        }
        else
        {
            upGradingPlayer2 = false;
            firstActivePlayer2 = false;
        }

        if (upGradingPlayer2 && !firstActivePlayer2)
        {
            QTEClass addInput = new QTEClass(gears.GetComponent<Gears>().player2.GetComponent<Player>().currentInputName, possibleInput[0].showInput);

            QTEClass addInputQ = new QTEClass(gears.GetComponent<Gears>().player2.GetComponent<Player>().currentInputName, possibleInput[1].showInput);

                AddInputToPlayer(addInput, player2QTE);
            AddInputToPlayer(addInputQ, player2QTE);

            firstActivePlayer2 = true;
        }
    }
    
    
    
    
    

    public void Percent(int i, List<QTEClass> o, bool fromPlayer1, GameObject go) //fallait mettre un gameobject a la place de bool -___-(plus le temp)
    {
        if (fromPlayer1)
        {
            o[i].percent = gears.GetComponent<Gears>().player1.GetComponent<Player>().percentOfHit;
        }
        else
        {
            o[i].percent = gears.GetComponent<Gears>().player2.GetComponent<Player>().percentOfHit;
        }

        if (o[i].percent <= o[i].perfectD.x && o[i].percent >= o[i].perfectD.y)
        {
            InputPressed(i, "Perfect", o, fromPlayer1);
            go.GetComponent<Player>().speedIncrease += o[i].speedsPerfect.x;
            go.GetComponent<Player>().speed += o[i].speedsPerfect.y;
        }else if (o[i].percent < o[i].greatD.x && o[i].percent >= o[i].greatD.y)
        {
            InputPressed(i, "Great", o, fromPlayer1);
            go.GetComponent<Player>().speedIncrease += o[i].speedsGreat.x;
            go.GetComponent<Player>().speed += o[i].speedsGreat.y;
        }else if (o[i].percent < o[i].goodD.x && o[i].percent >= o[i].goodD.y)
        {
            InputPressed(i, "Good", o, fromPlayer1);
            go.GetComponent<Player>().speedIncrease += o[i].speedsGood.x;
            go.GetComponent<Player>().speed += o[i].speedsGood.y;
        }else
        {
            InputPressed(i, "Fail", o, fromPlayer1);
        }
    }

    public void InputPressed(int i, string s, List<QTEClass> o, bool player1)
    {
        o[i].done = true;
        Destroy(o[i].inputShowed);
        
        int q = 0;
        switch (s)
        {
            case "Perfect" :
                q = 0;
                break;
            case "Great" :
                q = 1;
                break;
            case "Good" :
                q = 2;
                break;
            case "Fail" :
                q = 3;
                break;
        }

        FeedBackQualitySelect(q, player1);
        
        gears.GetComponent<Gears>().soundManager.GetComponent<SoundsManager>().PlaySound(s);
    }
    
    public void FeedBackQualitySelect(int i, bool b) //true pour le joueur1
    {
        if (b)
        {
            Vector2 inputPos1 = Camera.main.WorldToScreenPoint(feedBackPosWorldPlayer1.transform.position);
            feedBackInGamePlayer1 = Instantiate(feedBackQuality[i]);
            feedBackInGamePlayer1.transform.SetParent(gears.GetComponent<Gears>().canvasMain.transform);
            feedBackInGamePlayer1.transform.position = inputPos1;
        }
        else
        {
            Vector2 inputPos2 = Camera.main.WorldToScreenPoint(feedBackPosWorldPlayer2.transform.position);
            feedBackInGamePlayer2 = Instantiate(feedBackQuality[i]);
            feedBackInGamePlayer2.transform.SetParent(gears.GetComponent<Gears>().canvasMain.transform);
            feedBackInGamePlayer2.transform.position = inputPos2;
        }
    }

    public void AddInputToPlayer(QTEClass input, List<QTEClass> o)
    {
        if (o.Count >= 2)
        {
            ClearList(o);
        }
        
        o.Add(input);
    }

    public void ClearList(List<QTEClass> o2)
    {
        Destroy(o2[0].inputShowed);
        Destroy(o2[1].inputShowed);
        o2.Clear();
    }
}
