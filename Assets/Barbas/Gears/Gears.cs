﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gears : MonoBehaviour
{
    public GameObject player1;
    public GameObject[] showFeedBackPlayer1 = new GameObject[8];

    public GameObject player2;
    public GameObject[] showFeedBackPlayer2 = new GameObject[8];

    public string sceneToLoadName;
    public Canvas canvasMain;
    public GameObject soundManager;

    public GameObject qTEManager;
    public GameObject cameraManager;
    public Camera cam;
    public float endDistance;
    public bool getDistance;

    public GameObject pausePanel;
    public GameObject menuPanel;
    private Vector3 menuPanelBaseRectTransform;
    public GameObject selection;
    public GameObject camMan;

    public bool paused;

    public int highscore;

    void Start()
    {
        cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

        //soundManager.GetComponent<SoundsManager>().PlaySound("Menu");
        menuPanelBaseRectTransform = menuPanel.GetComponent<RectTransform>().localPosition;
        LoadMenu();
    }

    void FixedUpdate()
    {
        if (!player1 || !player2) //trouver les jouueur
        {
            if (GameObject.FindGameObjectWithTag("Player"))
            {
                player1 = GameObject.FindGameObjectWithTag("Player");
                showFeedBackPlayer1[0] = player1.transform.GetChild(0).gameObject;
                showFeedBackPlayer1[1] = player1.transform.GetChild(1).gameObject;
                showFeedBackPlayer1[2] = player1.transform.GetChild(2).gameObject;
                showFeedBackPlayer1[3] = player1.transform.GetChild(3).gameObject;
                showFeedBackPlayer1[4] = player1.transform.GetChild(4).gameObject;
                showFeedBackPlayer1[5] = player1.transform.GetChild(5).gameObject;
                showFeedBackPlayer1[6] = player1.transform.GetChild(6).gameObject;
                showFeedBackPlayer1[7] = player1.transform.GetChild(7).gameObject;

                if (GameObject.FindGameObjectWithTag("Player2"))
                {
                    player2 = GameObject.FindGameObjectWithTag("Player2");
                    showFeedBackPlayer2[0] = player2.transform.GetChild(0).gameObject;
                    showFeedBackPlayer2[1] = player2.transform.GetChild(1).gameObject;
                    showFeedBackPlayer2[2] = player2.transform.GetChild(2).gameObject;
                    showFeedBackPlayer2[3] = player2.transform.GetChild(3).gameObject;
                    showFeedBackPlayer2[4] = player2.transform.GetChild(4).gameObject;
                    showFeedBackPlayer2[5] = player2.transform.GetChild(5).gameObject;
                    showFeedBackPlayer2[6] = player2.transform.GetChild(6).gameObject;
                    showFeedBackPlayer2[7] = player2.transform.GetChild(7).gameObject;
                }
            }   
        }
    } //trouver les joueurs
    
    void Update()
    {
        if (Input.GetButtonDown("Cancel")) //PAuse
        {
            Pause();
        }

        if (cameraManager)
        {
            if (cameraManager.GetComponent<CameraScript>().distanceCameraPlayer >= 0.6f
            ) //minimum 6 pour scale 10 10   ZOOMZ over time
            {
                cameraManager.GetComponent<CameraScript>().distance -= Time.deltaTime * 0.02f;
            }

            if (cameraManager.GetComponent<CameraScript>().distanceCameraPlayer <= 0.5f && !getDistance &&
                cameraManager.GetComponent<CameraScript>().distanceCameraPlayer != 0
            ) //stoper le zoom et garder une distance
            {
                endDistance = cameraManager.GetComponent<CameraScript>().distance;
                getDistance = true;
            }

            if (getDistance)
            {
                cameraManager.GetComponent<CameraScript>().distance = endDistance;
            }


            if (player1) //trouver l'objet ene enfant du joueur le plus proche(pour instancier un text dessus)
            {
                float dist1 = Mathf.Infinity;

                foreach (var go in showFeedBackPlayer1)
                {
                    if (Vector3.Distance(cameraManager.transform.position, go.transform.position) < dist1)
                    {
                        qTEManager.GetComponent<QTEManager>().feedBackPosWorldPlayer1 = go;
                        qTEManager.GetComponent<QTEManager>().showTextHere = go.transform;
                        dist1 = Vector3.Distance(cameraManager.transform.position, go.transform.position);
                    }
                }

                float dist2 = Mathf.Infinity;

                foreach (var go in showFeedBackPlayer2)
                {
                    if (Vector3.Distance(cameraManager.transform.position, go.transform.position) < dist2)
                    {
                        qTEManager.GetComponent<QTEManager>().feedBackPosWorldPlayer2 = go;
                        qTEManager.GetComponent<QTEManager>().showTextHerePlayer2 = go.transform;
                        dist2 = Vector3.Distance(cameraManager.transform.position, go.transform.position);
                    }
                }
            }
        }
    }

    public void LoadMenu() //charger la scene menu avec déplacements du menu
    {
        camMan.SetActive(true);
        StopPause();
        menuPanel.SetActive(true);
        menuPanel.GetComponent<RectTransform>().localPosition = menuPanelBaseRectTransform;//new Vector3(-1000, -9,0);//position locale //new Vector3(-500, 375,0);//position world
        SceneManager.LoadScene("MenuScene");
        menuPanel.transform.DOMove(new Vector3(505, 384, 0), 1.5f).OnComplete(SelectionManagement);
    }

    public void SelectionManagement()
    {
        selection.SetActive(true);
        menuPanel.GetComponentInChildren<DotWeenScale>().enabled = true;
    }

    public void Pause() // pause/unpause
    {
        if (!paused)
        {
            Time.timeScale = 0;
            pausePanel.SetActive(true);
        }
        else
        {
            pausePanel.SetActive(false);
            Time.timeScale = 1;
        }

        paused = !paused;
    }

    public void StopPause()
    {
        if (paused)
        {
            pausePanel.SetActive(false);
            Time.timeScale = 1;
        }
    }

    public void LoadGame() //charger le jeu
    {
        //cameraManager.transform.position = new Vector3(0.5f, 5, 5);
        camMan.SetActive(false);
        SceneManager.LoadScene(sceneToLoadName);
        menuPanel.SetActive(false);
        soundManager.GetComponent<SoundsManager>().StopAllSound();
        soundManager.GetComponent<SoundsManager>().PlaySound("Continue");
    }
}
