﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UIElements;

public class CameraScript : MonoBehaviour
{
    public GameObject gears;
    
    public GameObject CameraGameObject;

    public GameObject cameraGoingBack;
    public Vector3 tran;
    public float zoomOutDuration;

    public GameObject mainPlayer;
    public GameObject playerBehind;
    
    public Transform centerOfTheCube;
    public Vector3 centerOfTheCube3 = Vector3.zero;

    public float distance;
    public float distanceCameraPlayer;

    void Start()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");
        gears.GetComponent<Gears>().cameraManager = gameObject;
        gears.GetComponent<Gears>().cam = transform.GetChild(0).GetComponent<Camera>();
    }
    
    void Update()
    {
        if (gears.GetComponent<Gears>().player1.GetComponent<Player>().distanceTravelled >=
            gears.GetComponent<Gears>().player2.GetComponent<Player>().distanceTravelled)
        {
            mainPlayer = gears.GetComponent<Gears>().player1;
            playerBehind = gears.GetComponent<Gears>().player2;
        }
        else
        {
            playerBehind = gears.GetComponent<Gears>().player1;
            mainPlayer = gears.GetComponent<Gears>().player2;
        }
        

        if (mainPlayer != null)
        {
            Vector3 lerpPlayers = Vector3.Lerp(mainPlayer.transform.position, playerBehind.transform.position, 0.12f); //pour mettre la cam entre les deux joueur
            
            distanceCameraPlayer = Vector3.Distance(CameraGameObject.transform.position, lerpPlayers);
            
            transform.LookAt(lerpPlayers);

            Vector3 directionCenterPlayer = lerpPlayers - centerOfTheCube3;
            Vector3 follow = directionCenterPlayer.normalized * distance / distanceCameraPlayer * 0.98f; //la distance entre le point sur le cube et sont centre varie
            

            Vector3 smoothedPosition;
            /*if (Mathf.Sign(transform.position.x) == 1)
            { 
                smoothedPosition = Vector3.Lerp(transform.position, 
                    follow + new Vector3(1.5f,0,1.5f), 0.05f * distance * 0.05f);   //Smooth Camera 0.125f
            }
            else
            {
                 smoothedPosition = Vector3.Lerp(transform.position, 
                    follow + new Vector3(-1.5f,0,-1.5f), 0.05f * distance * 0.05f);   //Smooth Camera 0.125f
            }*/

            smoothedPosition = Vector3.Lerp(transform.position,
                follow, 0.05f * distance * 0.05f);

            transform.position = smoothedPosition;
        }
    }
    
    public void CameraZoomOut()
    {
        //à faire pendant un boost
        tran = CameraGameObject.transform.localPosition;

        Vector3 testi = Vector3.Lerp(transform.localPosition,
            cameraGoingBack.transform.localPosition, 0.5f);

        CameraGameObject.transform.DOLocalMove(testi, zoomOutDuration).OnComplete(CameraEffect);
    }
    public void CameraEffect() //remettre la camera a sa position après un zoomout
    {
        CameraGameObject.transform.GetChild(0).gameObject.transform.DOLocalMove(tran, 0.3f);
    }

    public void ShakeCamera()
    {
        CameraGameObject.transform.GetChild(0).gameObject.transform.DOShakePosition(1f, 1, 1, 0, false, false);
    }
}
