﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class MenuCameraScript : MonoBehaviour
{
    public float zoomOutPower;
    public GameObject cameraGoingBack;
    public Vector3 tran;
    public int callNext;
    public float globalDuration;

    void Start()
    {
        RotateDown();
    }
    
    void Update()
    {
        
    }
    
    public void RotateDown()
    {
        Quaternion test = transform.rotation * Quaternion.Euler(-90, 0, 0);
        
        callNext = Random.Range(0, 3);
        switch (callNext)
        {
            case 0 :
                transform.DORotateQuaternion(test, globalDuration).OnComplete(RotateUp);
                break;
            case 1 :
                transform.DORotateQuaternion(test, globalDuration).OnComplete(RotateLeft);
                break;
            case 2 :
                transform.DORotateQuaternion(test, globalDuration).OnComplete(RotateRight);
                break;
        }
        
        CameraZoomOut();
    }
    
    public void RotateUp()
    {
        Quaternion test = transform.rotation * Quaternion.Euler(90, 0, 0);
        
        callNext = Random.Range(0, 3);
        switch (callNext)
        {
            case 0 :
                transform.DORotateQuaternion(test, globalDuration).OnComplete(RotateDown);
                break;
            case 1 :
                transform.DORotateQuaternion(test, globalDuration).OnComplete(RotateLeft);
                break;
            case 2 :
                transform.DORotateQuaternion(test, globalDuration).OnComplete(RotateRight);
                break;
        }

        CameraZoomOut();
    }
    public void RotateLeft()
    {
        Quaternion test = transform.rotation * Quaternion.Euler(0, 90, 0);
        
        callNext = Random.Range(0, 2);
        switch (callNext)
        {
            case 0 :
                transform.DORotateQuaternion(test, globalDuration).OnComplete(RotateUp);
                break;
            case 1 :
                transform.DORotateQuaternion(test, globalDuration).OnComplete(RotateDown);
                break;
            case 3 :
                transform.DORotateQuaternion(test, globalDuration).OnComplete(RotateRight);
                break;
        }
        
        CameraZoomOut();
    }
    
    public void RotateRight()
    {
        Quaternion test = transform.rotation * Quaternion.Euler(0, -90, 0);
        
        callNext = Random.Range(0, 2);
        switch (callNext)
        {
            case 0 :
                transform.DORotateQuaternion(test, globalDuration).OnComplete(RotateUp);
                break;
            case 1 :
                transform.DORotateQuaternion(test, globalDuration).OnComplete(RotateLeft);
                break;
            case 3 :
                transform.DORotateQuaternion(test, globalDuration).OnComplete(RotateDown);
                break;
        }

        CameraZoomOut();
    }
    
    public void CameraZoomOut()
    {
        tran = transform.GetChild(0).gameObject.transform.localPosition;

        Vector3 testi = Vector3.Lerp(transform.GetChild(0).gameObject.transform.localPosition,
            cameraGoingBack.transform.localPosition, 0.5f);

        transform.GetChild(0).gameObject.transform.DOLocalMove(testi, zoomOutPower);
    }
}
