﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CameraRotScript : MonoBehaviour
{
    public GameObject mainPlayer;
    public GameObject playerBehind;

    public GameObject cameraGoingBack;
    public Vector3 tran;
    public float zoomOutPower = 0.3f;

    void Start()
    {
        
    }
    
    void Update()
    {
        if (Input.GetButtonDown("Q"))
        {
            RotateLeft();
        }
        
        if (Input.GetButtonDown("D"))
        {
           RotateRight();
        }

        if (Input.GetButtonDown("S"))
        {
            RotateDown();
        }

        if (Input.GetButtonDown("Z"))
        {
            RotateUp();
        }

        /*Vector3 lerpPlayers = Vector3.Lerp(mainPlayer.transform.position, playerBehind.transform.position, 0.2f); //pour mettre la cam entre les deux joueur
        
        Vector3 test = Vector3.Lerp(transform.position, 
            new Vector3(lerpPlayers.x, lerpPlayers.y, lerpPlayers.z), 0.125f);*/
        if (mainPlayer != null)
        {
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, 
                new Vector3(mainPlayer.transform.position.x, mainPlayer.transform.position.y, mainPlayer.transform.position.z), 0.01f);   //Smooth Camera 0.125f
            
            transform.position = smoothedPosition;
        }
    }

    public void RotateDown()
    {
        Quaternion test = transform.rotation * Quaternion.Euler(-90, 0, 0);
        transform.DORotateQuaternion(test, 1f);
        
        //transform.eulerAngles = test.eulerAngles;

        //recule camera
        CameraZoomOut();
    }
    
    public void RotateUp()
    {
        Quaternion test = transform.rotation * Quaternion.Euler(90, 0, 0);
        transform.DORotateQuaternion(test, 1f);
        
        //transform.eulerAngles = test.eulerAngles;
        
        CameraZoomOut();
    }
    public void RotateLeft()
    {
        Quaternion test = transform.rotation * Quaternion.Euler(0, 90, 0);
        transform.DORotateQuaternion(test, 1f);
        
        CameraZoomOut();
    }
    
    public void RotateRight()
    {
        Quaternion test = transform.rotation * Quaternion.Euler(0, -90, 0);
        transform.DORotateQuaternion(test, 1f);
        
        //Mathf.Approximately() //c tellement op retourn vrai si les deux valeur comparé sont proche
        
       CameraZoomOut();
    }

    public void CameraZoomOut()
    {
        //à faire pendant un boost
        tran = transform.GetChild(0).gameObject.transform.localPosition;

        Vector3 testi = Vector3.Lerp(transform.GetChild(0).gameObject.transform.localPosition,
            cameraGoingBack.transform.localPosition, 0.5f);

        transform.GetChild(0).gameObject.transform.DOLocalMove(testi, zoomOutPower).OnComplete(CameraEffect);
    }
    public void CameraEffect()
    {
        transform.GetChild(0).gameObject.transform.DOLocalMove(tran, 0.3f);
    }
}
