﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour
{
    public bool withTimer;
    public float timer;
    
    void Start()
    {
        if (withTimer)
        {
            Destroy(gameObject, timer);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    
    void Update()
    {
        
    }
}
