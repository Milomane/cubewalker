﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class DotWeenScale : MonoBehaviour
{
    public Vector3 baseScale;
    public Vector3 scaleTo;
    
    public Vector3 basePos;
    public float offSet;
    public bool leftToRight;

    public string indication = "i = 1 : scale, i = 2 : bouger de gauche à droite";
    public int i;
    
    void Start()
    {
        switch (i)
        {
            case 1 :
                baseScale = transform.localScale;
                ScaleUp();
                break;
            case 2 :
               // basePos = GetComponent<RectTransform>().localPosition;
                basePos = transform.position;
                MooveLeft();
                break;
            default:
                print("not SetUp");
                break;
        }
    }
    
    void Update()
    {
        
    }

    public void ScaleUp()
    {
        transform.DOScale(scaleTo, 0.8f).OnComplete(ScaleBase);
    }

    public void ScaleBase()
    {
        transform.DOScale(baseScale, 0.8f).OnComplete(ScaleUp);
    }


    public void MooveLeft()
    {
        if (leftToRight)
        {
            transform.DOMove(basePos + new Vector3(-offSet, 0, 0), 0.6f).OnComplete(MoovRight);
        }
        else
        {
            transform.DOMove(basePos, 1).OnComplete(MoovRight);
        }
    }

    public void MoovRight()
    {
        if (leftToRight)
        {
            transform.DOMove(basePos, 1).OnComplete(MooveLeft);
        }
        else
        {
            transform.DOMove(basePos + new Vector3(offSet,0,0), 0.2f).OnComplete(MooveLeft);
        }
    }
}
