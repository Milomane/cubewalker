﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class DTUpDown : MonoBehaviour
{
    public Vector3 basePos;
    public float offSet;
    public bool reverse;
    
    void Start()
    {
        basePos = transform.position;
        MooveUp();
    }
    
    void Update()
    {
        
    }

    public void MooveUp()
    {
        if (reverse)
        {
            transform.DOMove(basePos, 1).OnComplete(MooveDown);
        }
        else
        {
            transform.DOMove(basePos + new Vector3(0, offSet, 0), 0.4f).OnComplete(MooveDown);
        }
    }

    public void MooveDown()
    {
        if (reverse)
        {
            transform.DOMove(basePos + new Vector3(0,-offSet,0), 0.4f).OnComplete(MooveUp);
        }
        else
        {
            transform.DOMove(basePos, 1).OnComplete(MooveUp);
        }
    }
}
