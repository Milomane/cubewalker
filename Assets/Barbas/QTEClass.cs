﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

[System.Serializable]
public class QTEClass
{
    public string input;

    public GameObject showInput;
    public GameObject inputShowed;
    public bool textShowedBool;

    public Color qteColor;
    public float percent;
    public float failD = 0.75f;
    public Vector2 goodD = new Vector2(0.75f, 0.5f); //Vector3: x = pourcentage max y = pourcentage min
    public Vector2 greatD = new Vector2(0.5f, 0.25f);
    public Vector2 perfectD = new Vector2(0.25f, 0);

    public Vector2 speedsGood = new Vector2(0.8f, 0.005f);
    public Vector2 speedsGreat = new Vector2(1.6f, 0.01f);
    public Vector2 speedsPerfect = new Vector2(2.5f, 0.015f);
    
    public Vector3 scoreS = new Vector3(10, 5, 3);//per = 10
    
    public bool done;

    public  QTEClass(string inp, GameObject show, float fai, Vector2 goo,Vector2 gr, Vector2 per, Vector2 speedgoo, Vector2 speedGr, Vector2 speedPer)
    {
        input = inp;
        showInput = show;
        
        failD = fai;
        goodD = goo;
        greatD = gr;
        perfectD = per;

        speedsGood = speedgoo;
        speedsGreat = speedGr;
        speedsPerfect = speedPer;
    }

    public QTEClass(string inp, GameObject show)
    {
        input = inp;
        showInput = show;
    }
}
