﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefineTileColor : MonoBehaviour
{
    public Color red;
    public Color yellow;
    public Color green;
    public Color blue;
}
