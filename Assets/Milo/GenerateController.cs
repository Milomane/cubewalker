﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateController : MonoBehaviour
{
    public int numberOfSideMax;
    
    public int numberOfSide;
    public List<GameObject> loadedSides;

    void Update()
    {
        numberOfSide = loadedSides.Count;

        if (numberOfSide > numberOfSideMax)
        {
            Debug.Log("ERROR");
        }
    }

    public void nextStep()
    {
        loadedSides[0].GetComponent<Side>().active = false;
        loadedSides.RemoveAt(0);

        loadedSides[(loadedSides.Count - 1)].GetComponent<Side>().active = true;
    }
}
