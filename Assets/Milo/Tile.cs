using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GlobalClasses;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public string inputName;
    
    public tileDir dir;

    public Animator animator;

    public OtherTile other;

    public bool noRandom;
    public bool notEmpty;

    public Color tileColor = Color.white;
    public bool colorSet;
    public bool colorUnset = true;
    public bool asQTE;

    public GameObject prefabIndicator;
    public GameObject indicator;

    private void Start()
    {
        indicator = Instantiate(prefabIndicator, transform.position, transform.rotation);
        indicator.GetComponent<Renderer>().sortingOrder = 1;
        indicator.GetComponent<Renderer>().material.DOFade(0, 0);
        
        if (!noRandom)
        {
            dir = tileDir.none;
        }
        
        animator = GetComponent<Animator>();
        
        Reset();
    }

    public void Update()
    {
        animator.SetBool("Empty", !notEmpty);
        switch (dir)
        {
            case tileDir.horizontal:
                animator.SetInteger("Dir", 1);
                notEmpty = false;
                break;
            case tileDir.vertical:
                animator.SetInteger("Dir", 2);
                notEmpty = false;
                break;
            case tileDir.upLeft:
                animator.SetInteger("Dir", 3);
                notEmpty = true;
                break;
            case tileDir.upRight:
                animator.SetInteger("Dir", 4);
                notEmpty = true;
                break;
            case tileDir.downLeft:
                animator.SetInteger("Dir", 5);
                notEmpty = true;
                break;
            case tileDir.downRight:
                animator.SetInteger("Dir", 6);
                notEmpty = true;
                break;
            case tileDir.none:
                animator.SetInteger("Dir", 0);
                notEmpty = false;
                break;
        }

        if (dir == tileDir.none || dir == tileDir.vertical || dir == tileDir.horizontal)
        {
            indicator.GetComponent<Renderer>().material.DOFade(0, 1);
            asQTE = false;
        }
        else
        {
            indicator.GetComponent<Renderer>().material.DOFade(1, 0);
            asQTE = true;
        }

        if (!colorSet && !colorUnset)
        {
            colorUnset = true;
            indicator.GetComponent<Renderer>().material.DOColor(Color.white, FindObjectOfType<Player>().originalSpeed / FindObjectOfType<Player>().speed);
            tileColor = Color.white;
        }
    }

    public void ChangeColor(Color color)
    {
        if (!colorSet)
        {
            colorSet = true;
            colorUnset = false;
            Debug.Log(FindObjectOfType<Player>().originalSpeed / FindObjectOfType<Player>().speed);
            indicator.GetComponent<Renderer>().material.DOColor(color, FindObjectOfType<Player>().originalSpeed / FindObjectOfType<Player>().speed);
        }
    }

    public void Reset()
    {
        if (!colorUnset)
        {
            colorSet = false;
            colorUnset = true;
            indicator.GetComponent<Renderer>().material.DOColor(Color.white, FindObjectOfType<Player>().originalSpeed / FindObjectOfType<Player>().speed);
            tileColor = Color.white;
        }
    }
}
