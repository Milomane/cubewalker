﻿using System;
using System.Collections;
using System.Collections.Generic;
using GlobalClasses;
using UnityEngine;
using Random = UnityEngine.Random;

public class Side : MonoBehaviour
{
    public bool firstSideActive;
    public bool active;
    public dir initDir = dir.down;

    public GameObject previousSide;
    public GameObject nextSide;
    public GameObject firstTile;
    public GameObject nextTile;

    private Tile tempTile;
    private BetweenTile tempBTile;

    private bool firstActive;

    private GenerateController controller;

    private int test;

    public void Start()
    {
        controller = GetComponentInParent<GenerateController>();
        ResetSide();
        if (firstSideActive)
        {
            active = true;
        }
    }

    public void Update()
    {
        if (active && !firstActive)
        {
            if ( (controller.numberOfSide <= controller.numberOfSideMax) )
            {
                CreateNew(firstTile, initDir);
            }
        }
    }

    public void CreateNew(GameObject tile, dir lastFacingDir)
    {
        controller.loadedSides.Add(gameObject);
        controller.numberOfSide++;
        
        firstActive = true;
        NewTile(tile, lastFacingDir);
    }

    public void NewTile(GameObject tile, dir lastFacingDir)
    {
        Debug.Log(tile + "   " + tile.tag);
        
        if (controller.numberOfSide > controller.numberOfSideMax)
        {
            return;
        }
        if (tile.GetComponent<Tile>() != null)
        {
            tempTile = tile.GetComponent<Tile>();
        }
        else
        {
            tempBTile = tile.GetComponent<BetweenTile>();
        }

        if (tempTile.tag != "InterTile")
        {
            int randDir = Mathf.RoundToInt(Random.Range(0, 3));
            nextTile = null; 
            dir comeFromDir = initDir;
            
            while (nextTile == null)
            {
                /*Debug.Log(tempTile.other.up.name);
                Debug.Log(tempTile.other.down.name);
                Debug.Log(tempTile.other.left.name);
                Debug.Log(tempTile.other.right.name);
                */
                
                if (randDir == 0)
                {
                    if (tempTile.other.up.GetComponent<Tile>())
                    {
                        if (tempTile.other.up.GetComponent<Tile>().dir == tileDir.none)
                        {
                            nextTile = tempTile.other.up;
                            comeFromDir = dir.down;
                        }
                    }
                    else
                    {
                        nextTile = tempTile.other.up;
                        comeFromDir = dir.down;
                    }
                }

                if (randDir == 1)
                {
                    if (tempTile.other.left.GetComponent<Tile>())
                    {
                        if (tempTile.other.left.GetComponent<Tile>().dir == tileDir.none)
                        {
                            nextTile = tempTile.other.left;
                            comeFromDir = dir.right;
                        }
                    }
                    else
                    {
                        nextTile = tempTile.other.left;
                        comeFromDir = dir.right;
                    }
                }

                if (randDir == 2)
                {
                    if (tempTile.other.right.GetComponent<Tile>())
                    {
                        if (tempTile.other.right.GetComponent<Tile>().dir == tileDir.none)
                        {
                            nextTile = tempTile.other.right;
                            comeFromDir = dir.left;
                        }
                    }
                    else
                    {
                        nextTile = tempTile.other.right;
                        comeFromDir = dir.left;
                    }
                }
                if (randDir == 3)
                {
                    if (tempTile.other.down.GetComponent<Tile>())
                    {
                        if (tempTile.other.down.GetComponent<Tile>().dir == tileDir.none)
                        {
                            nextTile = tempTile.other.down;
                            comeFromDir = dir.up;
                        }
                    }
                    else
                    {
                        nextTile = tempTile.other.down;
                        comeFromDir = dir.up;
                    }
                }

                if (nextTile == null)
                {
                    randDir++;
                    if (randDir >= 4) randDir = 0;
                }

                test++;

                if (test > 10)
                {
                    return;
                }
            }

            if (tile.GetComponent<Tile>())
            {
                DirOfTile(lastFacingDir, comeFromDir, tile.GetComponent<Tile>());
            }
            
            NewTile(nextTile, comeFromDir);
        }

        else
        
        {
            Debug.Log("Enter");
            
            GameObject otherSide;
            GameObject otherTile;
            dir comeFromDir;

            if (tempBTile.GetComponent<BetweenTile>().other1 != gameObject)
            {
                otherSide = tempBTile.GetComponent<BetweenTile>().other1.GetComponentInParent<Side>().gameObject;
                otherTile = tempBTile.GetComponent<BetweenTile>().other1;
                comeFromDir = tempBTile.GetComponent<BetweenTile>().ComeFromDir1;
                Debug.Log("OtherSide : " + otherSide);
            }
            else
            {
                otherSide = tempBTile.GetComponent<BetweenTile>().other2.GetComponentInParent<Side>().gameObject;
                otherTile = tempBTile.GetComponent<BetweenTile>().other2;
                comeFromDir = tempBTile.GetComponent<BetweenTile>().ComeFromDir2;
                Debug.Log("OtherSide : " + otherSide);
            }

            otherSide.GetComponent<Side>().previousSide = gameObject;
            otherSide.GetComponent<Side>().firstTile = otherTile;
            
            nextTile = otherTile;
            nextSide = otherSide;
            controller.loadedSides.Add(otherSide);
            

            otherTile.GetComponentInParent<Side>().initDir = comeFromDir;

            tile = otherTile;

            DirOfTile(lastFacingDir, comeFromDir, tile.GetComponent<Tile>());
            
            Debug.Log("MAISBORDELDEMERDE");
        }

        if (nextSide != null)
        {
            if (controller.numberOfSide <= controller.numberOfSideMax)
            {
                firstActive = false;
                nextSide.GetComponent<Side>().active = true;
            }
        }
    }

    public void DirOfTile(dir lastDir, dir newDir, Tile tile)
    {
        switch (lastDir)
        {
            case dir.up:
                if (newDir == dir.left)
                {
                    tile.dir = tileDir.upLeft;
                } 
                else if (newDir == dir.down)
                {
                    tile.dir = tileDir.vertical;
                }
                else
                {
                    tile.dir = tileDir.upRight;
                }
                break;
            case dir.down:
                if (newDir == dir.left)
                {
                    tile.dir = tileDir.downLeft;
                } 
                else if (newDir == dir.up)
                {
                    tile.dir = tileDir.vertical;
                }
                else
                {
                    tile.dir = tileDir.downRight;
                }
                break;
            case dir.left:
                if (newDir == dir.right)
                {
                    tile.dir = tileDir.horizontal;
                } 
                else if (newDir == dir.down)
                {
                    tile.dir = tileDir.downLeft;
                }
                else
                {
                    tile.dir = tileDir.upLeft;
                }
                break;
            case dir.right:
                if (newDir == dir.left)
                {
                    tile.dir = tileDir.horizontal;
                } 
                else if (newDir == dir.down)
                {
                    tile.dir = tileDir.downRight;
                }
                else
                {
                    tile.dir = tileDir.upRight;
                }
                break;
        }
    }

    public void ResetSide()
    {
        tempTile = null;
        tempBTile = null;
        
        active = false;
        firstActive = false;
        previousSide = null;
        foreach (Tile t in GetComponentsInChildren<Tile>())
        {
            t.dir = tileDir.none;
        }
    }
}
