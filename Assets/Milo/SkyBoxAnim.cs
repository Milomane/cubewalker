﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyBoxAnim : MonoBehaviour
{
    public Material[] skyboxMaterials;
    public float timeBetweenFrame;
    private float timer;
    private int frame;
    void Update()
    {
        timer -= Time.deltaTime;
        
        if (timer <= 0)
        {
            timer = timeBetweenFrame;
            RenderSettings.skybox = skyboxMaterials[frame];
            frame++;
            if (frame >= skyboxMaterials.Length)
            {
                frame = 0;
            }
        }
    }
}
