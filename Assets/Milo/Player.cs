using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GlobalClasses;
using UnityEditor.PackageManager;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class Player : MonoBehaviour
{
    public GameObject gears;
    
    public bool player1;
    public List<GameObject> playerPath;
    public List<GameObject> playerPathSave = new List<GameObject>();
    public float positionSensitivity = 0.01f;

    public float speed;
    public float speedIncrease = 1;
    public float originalSpeed;

    public float lineOffset;
    
    public Transform target;
    public Transform lastTarget;
    public float distanceToNextTarget;
    private bool targetApproch;
    public float percentDistanceSensitivity = 0.1f;
    public float percentOfHit;
    private bool death;

    public float distanceTravelled;
    
    public LayerMask cubeLayer;

    public string currentInputName;
    public string inputName0;
    public string inputName1;
    public string inputName2;
    public string inputName3;

    public GameObject otherPlayer;
    private ParticleSystem particle;
    public GameObject playerWin;
    
    void Start()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");
        
        particle = GetComponentInChildren<ParticleSystem>();
        particle.Stop();
        transform.rotation = Quaternion.Euler(Vector3.RotateTowards(transform.position, playerPath[0].transform.position, 5 * Time.deltaTime, 0));
        FillListSave();
        ChooseTarget();
        target = playerPath[0].transform;
        lastTarget = target;
        ApplyColor();
    }

    void Update()
    {
        if (speedIncrease > 1)
        {
            speedIncrease -= Time.deltaTime * 3;
        }
        
        if (Vector3.Distance(playerPath[0].transform.position, transform.position) < positionSensitivity)
        {
            transform.position = playerPath[0].transform.position;
            playerPath.RemoveAt(0);

            if (playerPath.Count == 0)
            {
                FillList();
            }
        }
        else
        {
            Vector3 toTarg = (playerPath[0].transform.position + -transform.position).normalized;
            transform.Translate(toTarg * speed * Time.deltaTime * speedIncrease);
            distanceTravelled += speed * Time.deltaTime;
        }


        //Debug.Log("Distance" + Vector3.Distance(target.transform.position, transform.position) + "   DistanceSensitivity" + percentDistanceSensitivity + 0.1f);
        
        
        
        if (targetApproch)
        {
            distanceToNextTarget = Vector3.Distance(target.transform.position, transform.position);
        }
        else
        {
            ChooseTarget();
            distanceToNextTarget = 1;
        }
        
        if (Vector3.Distance(target.transform.position, transform.position) < percentDistanceSensitivity)
        {
            targetApproch = true;
        }
        else if (Vector3.Distance(target.transform.position, transform.position) >= percentDistanceSensitivity)
        {
            targetApproch = false;
        }

        percentOfHit = distanceToNextTarget / 0.1f;

        /*
        // PLAYER DEATH
        RaycastHit hit;
        //Physics.Raycast(Camera.main.transform.position, transform.position - Camera.main.transform.position, out hit, 1, cubeLayer);
        
        Physics.Linecast(Camera.main.transform.position, transform.position - Camera.main.transform.position, out hit, cubeLayer);
        
        Debug.Log(hit.collider.name);
        
        if (Physics.Raycast(Camera.main.transform.position, transform.position - Camera.main.transform.position, 1, cubeLayer))
        {
            RaycastHit hit2;
            Physics.Raycast(Camera.main.transform.position, transform.position - Camera.main.transform.position, out hit2, 1, cubeLayer);
            
            Debug.Log(hit.collider.gameObject.name);
            
            if (hit.collider.gameObject.GetComponent<GenerateController>())
            {
                Debug.Log("Chui cacher");
            }
        }
        */
        
        currentInputName = target.GetComponent<Tile>().inputName;
        
        
        if (lastTarget != target)
        {
            Debug.Log(gameObject.name + "   Distance travelled : " + distanceTravelled + "    Other distance travelled : " + otherPlayer.GetComponent<Player>().distanceTravelled + "    is Player 1 : " + player1);
            Debug.Log(gameObject.name + "" + (distanceTravelled == otherPlayer.GetComponent<Player>().distanceTravelled && player1));
            
            if (distanceTravelled < otherPlayer.GetComponent<Player>().distanceTravelled)
            {
                Debug.Log("Reset");
                lastTarget.GetComponent<Tile>().Reset();
            }
            else if (distanceTravelled <= otherPlayer.GetComponent<Player>().distanceTravelled + 0.1f && distanceTravelled >= otherPlayer.GetComponent<Player>().distanceTravelled - 0.1f && player1)
            {
                Debug.Log("Reset");
                lastTarget.GetComponent<Tile>().Reset();
            }
            
            lastTarget = target;
        }
        
        // CheckFirstForColor
        ApplyColor();

        //Debug.Log(gameObject + "  GetcompomentTile  : " + playerPath[0].GetComponent<Tile>());
    }

    private void FixedUpdate()
    {
        if (!IsInView(Camera.main.gameObject, gameObject) && !death)
        {
            death = true;
            StartCoroutine(Death());
        }
    }

    public void FillList()
    {
        for (int i = 0; i < playerPathSave.Count; i++)
        {
            playerPath.Add(playerPathSave[i]);
        }
    }
    
    public void FillListSave()
    {
        for (int i = 0; i < playerPath.Count; i++)
        {
            playerPathSave.Add(playerPath[i]);
        }
    }

    public void ChooseTarget()
    {
        if (playerPath.Count != 0)
        {
            target = null;
            int i = 0;
            while (target == null)
            {
                
                if (playerPath[i].GetComponent<Tile>())
                {
                    if (playerPath[i].GetComponent<Tile>().notEmpty)
                    {
                        target = playerPath[i].transform;
                    }
                }
                
                /*
                if (playerPath[i].GetComponent<BetweenTile>())
                {
                    target = playerPath[i].transform;
                }
                */
                
                i++;
                if (i + 1 > playerPath.Count)
                {
                    return;
                }
            }
        }
    }
    
    private void ApplyColor()
    {
        if (distanceTravelled < otherPlayer.GetComponent<Player>().distanceTravelled)
        {
            int temp = 0;
            for (int i = 0; i <= 3; i++)
            {
                if (playerPath[i+temp].GetComponent<Tile>())
                {
                    if (playerPath[i+temp].GetComponent<Tile>().notEmpty)
                    {
                        Debug.Log("");
                        ChooseColor(playerPath[i+temp].GetComponent<Tile>());
                    }
                    else
                    {
                        i--;
                        temp++;
                    }
                }
                else
                {
                    i--;
                    temp++;
                }
            }
        }
        if (distanceTravelled <= otherPlayer.GetComponent<Player>().distanceTravelled + 0.01f && distanceTravelled >= otherPlayer.GetComponent<Player>().distanceTravelled + 0.01f && player1)
        {
            for (int i = 0; i <= 3; i++)
            {
                if (playerPath[i].GetComponent<Tile>())
                    Debug.Log("");
                    ChooseColor(playerPath[i].GetComponent<Tile>());
            }
        }
    }
    
    public void ChooseColor(Tile tile)
    {
        string inputName = "";
        int i = Random.Range(0, 3);
        Color color = Color.white;
        var tileColor = FindObjectOfType<DefineTileColor>();
        switch (i)
        {
            case 0:
                color = tileColor.blue;
                inputName = inputName0;
                break;
            case 1:
                color = tileColor.red;
                inputName = inputName1;
                break;
            case 2:
                color = tileColor.yellow;
                inputName = inputName2;
                break;
            case 3:
                color = tileColor.green;
                inputName = inputName3;
                break;
        }
        
        tile.inputName = inputName;
        tile.ChangeColor(color);
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(Camera.main.transform.position, (transform.position - Camera.main.transform.position) * 0.98f);
    }
    
    private bool IsInView(GameObject origin, GameObject toCheck)
    {
        RaycastHit hit;
        Vector3 heading = toCheck.transform.position - origin.transform.position;
        Vector3 direction = heading.normalized;// / heading.magnitude;
         
        if (Physics.Linecast(Camera.main.transform.position, toCheck.GetComponentInChildren<Renderer>().bounds.center, out hit))
        {
            if (hit.transform.name != toCheck.name)
            {
                /* -->
                Debug.DrawLine(cam.transform.position, toCheck.GetComponentInChildren<Renderer>().bounds.center, Color.red);
                Debug.LogError(toCheck.name + " occluded by " + hit.transform.name);
                */
                Debug.Log(toCheck.name + " occluded by " + hit.transform.name);
                if (hit.transform.name == "Cube")
                {
                    return false;
                }
            }
        }
        return true;
    }

    public IEnumerator Death()
    {
        speed = 0;
        particle.Play();
        playerWin.SetActive(true);
        transform.DOScale(new Vector3(0.4f, 0.4f, 0.4f), 1);
        yield return new WaitForSeconds(1);
        transform.DOScale(new Vector3(0, 0, 0), 1);
        yield return new WaitForSeconds(4);
        particle.Stop();
        playerWin.SetActive(false);

        /*if (PlayerPrefs.GetInt("HIGHSCORE") < FindObjectOfType<Gears>().highscore)
            PlayerPrefs.SetInt("HIGHSCORE", FindObjectOfType<Gears>().highscore);
        */

        if (gears.GetComponent<Gears>().qTEManager.GetComponent<QTEManager>().test.Count == 2)
        {
            gears.GetComponent<Gears>().qTEManager.GetComponent<QTEManager>().ClearList(gears.GetComponent<Gears>().qTEManager.GetComponent<QTEManager>().test);
        }
        if (gears.GetComponent<Gears>().qTEManager.GetComponent<QTEManager>().player2QTE.Count == 2)
        {
            gears.GetComponent<Gears>().qTEManager.GetComponent<QTEManager>().ClearList(gears.GetComponent<Gears>().qTEManager.GetComponent<QTEManager>().player2QTE);
        }
        
        FindObjectOfType<Gears>().LoadGame();
        yield return null;
    }
}
