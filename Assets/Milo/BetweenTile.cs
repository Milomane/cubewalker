﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GlobalClasses;

public class BetweenTile : MonoBehaviour
{
    public GameObject other1;
    public GlobalClasses.dir ComeFromDir1;
    public GameObject other2;
    public GlobalClasses.dir ComeFromDir2;
}
