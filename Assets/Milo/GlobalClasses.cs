﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GlobalClasses
{
    public enum tileDir
    {
        vertical,
        horizontal,
        upLeft,
        upRight,
        downLeft,
        downRight,
        none
    }

    public enum dir
    {
        up,
        down,
        left,
        right
    }
    
    [System.Serializable]
    public class OtherTile
    {
        public GameObject up;
        public GameObject down;
        public GameObject left;
        public GameObject right;
    }
}